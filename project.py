import pygame
import pygame_menu
import sys
import random
import time
from playsound import playsound

#hlavní herní smyčka
def gameloop():

	global snake, direction, snake_len, score, speed, apple_image, mushroom_counter

	score = 0

	#počáteční pozice hada
	x1 = 300
	y1 = 300

	#počáteční pohyb hada
	x2 = 24
	y2 = 0
	direction = 'Right'
	
	snake = []
	snake_len = 1
	
	running = True

	#proměnná, která se změní po snězení houby
	mushroom_counter = 100

	#položení jablka a houby
	place_apple()
	place_mushroom()

	while running:
		#pozadí
		screen.blit(pygame.image.load('background.png'), [0, 0])

		for event in pygame.event.get():
			#tlačítko pro zavření okna
			if event.type == pygame.QUIT:
				running = False
				pygame.quit()
				sys.exit()

			#pohyb hada podle zmáčknutých kláves
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_LEFT:
					#pokud had snědl houbu - obrácené řízení
					if mushroom_counter != 100:
						x2 = 24
						y2 = 0

					#pokud had nesnědl houbu
					else:
						x2 = -24
						y2 = 0

					direction = 'Left'

				elif event.key == pygame.K_RIGHT:
					if mushroom_counter != 100:
						x2 = -24
						y2 = 0

					else:
						x2 = 24
						y2 = 0
					
					direction = 'Right'
				
				elif event.key == pygame.K_UP:
					if mushroom_counter != 100:
						y2 = 24
						x2 = 0
				
					else:
						y2 = -24
						x2 = 0
				
					direction = 'Up'
				
				elif event.key == pygame.K_DOWN:
					if mushroom_counter != 100:
						y2 = -24
						x2 = 0
				
					else:
						y2 = 24
						x2 = 0
				
					direction = 'Down'
				
				#po zmáčknutí escape návrat do hlavního menu
				elif event.key == pygame.K_ESCAPE:
					menu_main()

		#poloha hada po změnění směru
		x1 += x2
		y1 += y2

		#nastavení, aby had procházel okraji obrazovky
		if x1 < 0:
			x1 = width - 23
		
		if x1 > (width - 23):
			x1 = 0
		
		if y1 < 0:
			y1 = height - 23
		
		if y1 > (height - 23):
			y1 = 0

		#pozice hlavy
		head_position = [x1, y1, x1 + 24, y1 + 24]

		snake.append(list((head_position[0], head_position[1])))

		#přidání textu se skóre
		score_label = 'Score:' + str(score)
		font = pygame.font.Font(None, 40)
		text = font.render(str(score_label), 1, (0, 0, 0))
		screen.blit(text, (330, 2))
		
		#přidání jablka a houby
		screen.blit(apple, (appleX, appleY))
		screen.blit(mushroom, (mushroomX, mushroomY))

		colour = (0, 0, 0)
		counter = 0

		for item in snake:
			#po snězení houby se změní barva hada
			if mushroom_counter != 100:
				r = lambda: random.randint(0, 255)
				colour = '#%02X%02X%02X' % (r(), r(), r())

			#vykreslení hada
			pygame.draw.rect(screen, colour, [item[0], item[1], 24, 24])
			
			#pozice každého čtverečku v hadovi
			item_position = [item[0], item[1], item[0] + 24, item[1] + 24]

			#game over pokud had 'kousne' sám sebe
			if not counter == (snake_len-1):
				if overlaping(item_position, head_position):
					if sound == 1:
						playsound('game_over.wav', False)
			
					running = False
					game_over()

			counter += 1
		
		#doba trvání vlivu houby
		if mushroom_counter != 100:
			mushroom_counter += 1
		
		#update herní obrazovky
		pygame.display.update()

		#snězení jablka
		if overlaping(head_position, apple_position):
			if sound == 1:
				playsound('jablicko.wav', False)
			
			place_apple()
			place_mushroom()
			grow_snake()

		#snězení houby
		if overlaping(head_position, mushroom_position):
			if sound == 1:
				playsound('houbicky.wav', False)
			
			mushroom_counter = 0
			
			place_apple()
			place_mushroom()

		#vymazání prvního prvku hada - zajištění pohybu
		snake.pop(0)
		
		#rychlost pohybu (překreslování obrazovky)
		time.sleep(speed)


#rychlost hada podle výběrového listu v nastavení
def speed_of_snake(value, velocity):
	global speed
	
	speed = velocity
	return speed


#zvuk podle výběrového listu v nastavení
def play_sound(value, music):
	global sound
	
	sound = music
	return sound


#funkce pro růst hada
def grow_snake():
	
	global snake, snake_len, score
	score += 1
	snake_len += 1
	last_elem = len(snake)-1
	
	#růst hada v závislosti na směru pohybu
	if direction == 'Left':
		snake.append(list((snake[last_elem][0] + 24, snake[last_elem][1])))
	
	elif direction == 'Right':
		snake.append(list((snake[last_elem][0] - 24, snake[last_elem][1])))
	
	elif direction == 'Up':
		snake.append(list((snake[last_elem][0], snake[last_elem][1] + 24)))
	
	elif direction == 'Down':
		snake.append(list((snake[last_elem][0], snake[last_elem][1] - 24)))


#funkce pro vykreslení jablka
def place_apple():
	global apple, appleX, appleY, apple_position, mushroom_counter
	
	#vykreslení červeného jablka
	if mushroom_counter == 100:
		apple_image = 'apple.png'
	
	#pokud had snědl houbu, vykreslí se zelené jablko
	else:
		apple_image = 'green_apple.png'
	
	apple = pygame.image.load(apple_image)
	appleX = round(random.randint(0, width - 32) / 10.0) * 10
	appleY = round(random.randint(0, height - 32) / 10.0) * 10
	apple_position = [appleX, appleY, appleX + 32, appleY + 32]
	screen.blit(apple, (appleX, appleY))


#vykreslení houby
def place_mushroom():
	global mushroom, mushroomX, mushroomY, mushroom_position
	
	mushroom = pygame.image.load('mushroom.png')
	mushroomX = round(random.randint(0, width - 32) / 10.0) * 10
	mushroomY = round(random.randint(0, height - 32) / 10.0) * 10
	mushroom_position = [mushroomX, mushroomY, mushroomX + 32, mushroomY + 32]
	screen.blit(mushroom, (mushroomX, mushroomY))


#překrývání vykreslenách objektů
def overlaping(a, b):
	if a[0] < b[2] and a[2] > b[0] and a[1] < b[3] and a[3] > b[1]:
		return True
	
	return False


#nastavení hlavního menu
def menu_main():
	menu = pygame_menu.Menu(height, width, 'SNAKE GAME', theme=my_theme)
	menu.add_button('Play', gameloop)
	menu.add_vertical_margin(30)
	menu.add_button('Game instructions', menu_instructions)
	menu.add_vertical_margin(30)
	menu.add_button('Settings', menu_settings)
	menu.add_vertical_margin(30)
	menu.add_button('About', menu_about)
	menu.add_vertical_margin(30)
	menu.add_button('Exit', pygame_menu.events.EXIT)
	menu.mainloop(screen)


#nastavení menu s instrukcemi
def menu_instructions():
	instructions_menu = pygame_menu.Menu(height, width, 'GAME INSTRUCTIONS', theme=my_theme_2)
	instructions_menu.add_label('The snake likes apples, but mushrooms make him')
	instructions_menu.add_label('feel sick and disoriented. Try to avoid them!')
	instructions_menu.add_vertical_margin(30)
	instructions_menu.add_label('When the snake is under an influence of a mushroom,')
	instructions_menu.add_label('the apple suddenly becomes poisoneus!')
	instructions_menu.add_vertical_margin(30)
	instructions_menu.add_label('You can move the snake by using keys')
	instructions_menu.add_label('UP, DOWN, LEFT and RIGHT')
	instructions_menu.add_vertical_margin(30)
	instructions_menu.add_button('Back', menu_main)
	instructions_menu.mainloop(screen)


#nastavení informačního menu
def menu_about():
	about_menu = pygame_menu.Menu(height, width, 'ABOUT', theme=my_theme)
	about_menu.add_label('This game was created by Michaela Měrková.')
	about_menu.add_image('misa.png')
	about_menu.add_vertical_margin(50)
	about_menu.add_button('Back', menu_main)
	about_menu.mainloop(screen)


#nastavené menu s nastaveními
def menu_settings():
	settings_menu = pygame_menu.Menu(height, width, 'SETTINGS MENU', theme=my_theme)
	settings_menu.add_selector('Speed ', [('Normal', (0.03)), ('Fast', (0.01)), ('Slow ', (0.05))], onchange=speed_of_snake)
	settings_menu.add_vertical_margin(50)
	settings_menu.add_selector('Sound ', [('On', 1), ('Off', 2)], onchange=play_sound)
	settings_menu.add_vertical_margin(50)
	settings_menu.add_button('Back', menu_main)
	settings_menu.mainloop(screen)


#game over obrazovka
def game_over():
	game_over_menu = pygame_menu.Menu(height, width, '', theme=my_theme_2)
	game_over_menu.add_label('GAME OVER', font_size=80)
	game_over_menu.add_label('Your score: ' + str(score))
	game_over_menu.add_vertical_margin(50)
	game_over_menu.add_button('Back', menu_main)
	game_over_menu.mainloop(screen)

#hlavní okno + lišta
pygame.init()

#rozměry okna
width = 792
height = 480
screen = pygame.display.set_mode((width, height))

#popisek okna
pygame.display.set_caption('S N A K E')

#ikona okna
icon = pygame.image.load('snake1.png')
pygame.display.set_icon(icon)

#velikost čtverečku hada a základní rychlost
snake_size = 24
speed = 0.03

#zvuk v základu zapnutý
sound = 1

#téma s hadem
my_theme = pygame_menu.themes.Theme(background_color=pygame_menu.baseimage.BaseImage('menu_background.png'),
	menubar_close_button=False, widget_font_color=(0, 0, 0), title_font_color=(0, 0, 0), title_font='serif',
	title_background_color=(0, 0, 0), title_bar_style=pygame_menu.widgets.MENUBAR_STYLE_NONE,
	widget_font='serif')

#téma bez hada
my_theme_2 = pygame_menu.themes.Theme(background_color=pygame_menu.baseimage.BaseImage('new_background.png'),
	menubar_close_button=False, widget_font_color=(0, 0, 0), title_font_color=(0, 0, 0), title_font='serif',
	title_background_color=(0, 0, 0), title_bar_style=pygame_menu.widgets.MENUBAR_STYLE_NONE,
	widget_font='serif')

#spuštění hlavního menu
menu_main()